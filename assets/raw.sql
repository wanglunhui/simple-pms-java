create table `system_department`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `parent_id`            int unsigned default null comment '父级部门id',
    `leader`               varchar(255) default '' comment '领导',
    `phone_number`         varchar(255) default '' comment '联系电话',
    `email`                varchar(255) default '' comment '邮箱',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统部门表';
create table `system_dictionary`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统字典表';
create table `system_dictionary_detail`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `dictionary_id`        int unsigned not null comment '字典id',
    `key`                  varchar(255) default '' comment '键',
    `value`                varchar(255) default '' comment '值',
    `sort`                 int          default 0 comment '排序',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统字典详情表';
create table `system_job`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `code`                 varchar(255) default '' comment '代码',
    `name`                 varchar(255) default '' comment '名称',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统职位表';
create table `system_log`
(
    `id`          int unsigned not null auto_increment comment '自增id',
    `name`        varchar(255) default '' comment '名称',
    `user_id`     int unsigned default null comment '用户id,不是not null因为登录时没有id',
    `user_name`   varchar(255) default '' comment '用户名',
    `url`         varchar(255) default '' comment 'url',
    `method`      varchar(255) default '' comment '方法',
    `params`      text comment '参数',
    `time`        int unsigned default null comment '耗时,单位ms',
    `ip`          varchar(255) default '' comment 'ip地址',
    `log_type`    char(1)      default null comment '类型,0操作日志,1其他日志',
    `exception`   text comment '异常',
    `create_time` datetime     default null comment '创建日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统日志表';
create table `system_menu`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `parent_id`            int unsigned not null comment '父级菜单id,不能为外键否则顶级菜单id为0则没有对应的',
    `sort`                 int          default 0 comment '排序',
    `path`                 varchar(255) default '' comment '访问路径,为合法的URL时会自动在外链打开',
    `component`            varchar(255) default '' comment '组件文件位置',
    `icon`                 varchar(255) default '' comment '图标',
    `redirect`             varchar(255) default '' comment '重定向地址,一般为上级菜单重定向到下级的某个菜单,为noredirect时在面包屑中不可被点击',
    `hidden`               char(1)      default '0' comment '是否在侧边栏隐藏,一般像登录、404错误等不需要在侧边栏显示的需要设为true,默认为0,0否,1是',
    `always_show`          char(1)      default '1' comment '是否总是显示,为false时当顶级菜单只有一个子菜单则子菜单会代替顶级菜单来显示顶级菜单不会显示,为false则可让顶级菜单一致显示而子菜单总在其下面,为最顶级菜单时应为1,为最低级菜单时必须为0否则不会有页面而变成一个父级菜单,默认为0,0否,1是',
    `no_cache`             char(1)      default '0' comment '是否不缓存,一般经常变化的界面不需要缓存,默认为0,0否,1是',
    `breadcrumb`           char(1)      default '1' comment '是否在面包屑显示,默认为1,0否,1是',
    `affix`                char(1)      default '0' comment '是否固定在tags-view中,默认为0,0否,1是',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统菜单表';
create table `system_permission`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统权限表';
create table `system_role`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `data_scope`           char(1)      default '1' comment '数据权限,全部数据权限1,自定数据权限2,本部门数据权限3,本部门及以下数据权限4,仅本人数据权限5',
    `remark`               varchar(255) default '' comment '备注',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统角色表';
create table `system_role_department`
(
    `id`            int unsigned not null auto_increment comment '自增id',
    `role_id`       int unsigned default null comment '角色id',
    `department_id` int unsigned default null comment '部门id',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统角色部门关联表';
create table `system_role_menu`
(
    `id`      int unsigned not null auto_increment comment '自增id',
    `role_id` int unsigned default null comment '角色id',
    `menu_id` int unsigned default null comment '菜单id',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统角色菜单关联表';
create table `system_role_permission`
(
    `id`            int unsigned not null auto_increment comment '自增id',
    `role_id`       int unsigned default null comment '角色id',
    `permission_id` int unsigned default null comment '权限id',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统角色权限关联表';
create table `system_user`
(
    `id`                   int unsigned not null auto_increment comment '自增id',
    `name`                 varchar(255) default '' comment '名称',
    `nick_name`            varchar(255) default '' comment '昵称',
    `password`             varchar(255) default '' comment '密码',
    `salt`                 varchar(255) default '' comment '加密盐',
    `avatar_url`           varchar(255) default '' comment '头像url',
    `enable`               char(1)      default '1' comment '是否启用,0否,1是',
    `email`                varchar(255) default '' comment '邮箱',
    `phone_number`         varchar(255) default '' comment '联系电话',
    `sex`                  char(1)      default '0' comment '性别,0男,1女,2未知',
    `remark`               varchar(255) default '' comment '备注',
    `department_id`        int unsigned default null comment '部门id',
    `create_user_id`       int unsigned default null comment '创建用户id',
    `create_department_id` int unsigned default null comment '创建部门id',
    `create_time`          datetime     default null comment '创建日期',
    `update_time`          datetime     default null comment '修改日期',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统用户表';
create table `system_user_job`
(
    `id`      int unsigned not null auto_increment comment '自增id',
    `user_id` int unsigned default null comment '用户id',
    `job_id`  int unsigned default null comment '职位id',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统用户职位关联表';
create table `system_user_role`
(
    `id`      int unsigned not null auto_increment comment '自增id',
    `user_id` int unsigned default null comment '用户id',
    `role_id` int unsigned default null comment '角色id',
    primary key (`id`)
)engine=innodb default charset=utf8mb4 comment = '系统用户角色关联表';
