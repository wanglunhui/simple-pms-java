package indi.simple.pms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SimplePMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplePMSApplication.class, args);
    }

}
