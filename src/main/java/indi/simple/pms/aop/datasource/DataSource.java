package indi.simple.pms.aop.datasource;

import java.lang.annotation.*;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-04-19 17:45:00
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {

    DataSourceType value() default DataSourceType.MASTER;

}
