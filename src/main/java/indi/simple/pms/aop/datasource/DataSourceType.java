package indi.simple.pms.aop.datasource;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-04-19 17:45:40
 */
public enum DataSourceType {

    MASTER,
    SLAVE,

}
