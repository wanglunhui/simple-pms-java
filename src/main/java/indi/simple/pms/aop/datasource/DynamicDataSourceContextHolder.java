package indi.simple.pms.aop.datasource;

import lombok.extern.slf4j.Slf4j;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-04-19 17:48:43
 */
@Slf4j
public class DynamicDataSourceContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    public static void setDataSourceType(String dsType) {
        log.debug("切换到{}数据源", dsType);
        CONTEXT_HOLDER.set(dsType);
    }

    public static String getDataSourceType() {
        return CONTEXT_HOLDER.get();
    }

    public static void clearDataSourceType() {
        CONTEXT_HOLDER.remove();
    }

}
