package indi.simple.pms.aop.sqlslot;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;

/**
 * @Author: wanglunhui
 * @Date: 2021/4/13 21:33
 * @Description:
 */
@Aspect
@Component
public class SqlSlotAspect {

    /*@Autowired
    private ApplicationContext applicationContext;*/
    @Autowired
    private SqlSlotExpressionOperations sqlSlotExpressionOperations;
    private final StandardEvaluationContext standardEvaluationContext=new StandardEvaluationContext();
    private final ExpressionParser expressionParser=new SpelExpressionParser();
    //private static final ExpressionParser EXPRESSION_PARSER=new SpelExpressionParser();
    //private static final StandardEvaluationContext STANDARD_EVALUATION_CONTEXT=new StandardEvaluationContext();

    @PostConstruct
    public void init() {
        //standardEvaluationContext.setBeanResolver(new BeanFactoryResolver(applicationContext));
        standardEvaluationContext.setRootObject(sqlSlotExpressionOperations);

        /*Class<?> clz=SqlSlotExpressionOperations.class;
        Method[] methods=clz.getMethods();
        for (Method method:methods){
            STANDARD_EVALUATION_CONTEXT.registerFunction(method.getName(),method);
        }
        System.out.println(EXPRESSION_PARSER.parseExpression("#{sqlSlotExpressionOperations.dataScope()}").getValue(STANDARD_EVALUATION_CONTEXT));*/

        /*AnnotationConfigServletWebServerApplicationContext annotationConfigServletWebServerApplicationContext=(AnnotationConfigServletWebServerApplicationContext) applicationContext;
        ConfigurableListableBeanFactory configurableBeanFactory=annotationConfigServletWebServerApplicationContext.getBeanFactory();
        System.out.println(configurableBeanFactory.getBeanExpressionResolver().evaluate("#{sqlSlotExpressionOperations.dataScope()}",new BeanExpressionContext(configurableBeanFactory, null)));*/
    }

    @Pointcut("@annotation(indi.simple.pms.aop.sqlslot.SqlSlot)")
    public void sqlSlotPointCut() {
    }

    @Around("sqlSlotPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;
        Method method = methodSignature.getMethod();

        SqlSlot sqlSlotAnnotation = method.getAnnotation(SqlSlot.class);

        String sqlSlot=expressionParser.parseExpression(sqlSlotAnnotation.value()).getValue(standardEvaluationContext,String.class);

        if (StringUtils.hasLength(sqlSlot)) {
            SqlSlotContextHolder.setSqlSlot("(" + sqlSlot.substring(4) + ")"); // 前面的" or "去掉
        }

        try {
            return point.proceed();
        } finally {
            SqlSlotContextHolder.clearSqlSlot();
        }
    }

}
