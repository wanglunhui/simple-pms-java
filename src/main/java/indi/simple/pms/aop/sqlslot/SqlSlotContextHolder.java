package indi.simple.pms.aop.sqlslot;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-28 17:24:44
 */
public class SqlSlotContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    public static void setSqlSlot(String sqlSlot) {
        CONTEXT_HOLDER.set(sqlSlot);
    }

    public static String getSqlSlot() {
        return CONTEXT_HOLDER.get();
    }

    public static void clearSqlSlot() {
        CONTEXT_HOLDER.remove();
    }

}
