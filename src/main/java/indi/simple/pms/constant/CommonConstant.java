package indi.simple.pms.constant;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-30 18:06:23
 */
public class CommonConstant {

    public static final Charset DEFAULT_CHARSET=StandardCharsets.UTF_8;
    public static final String ERROR_OCCUR="发生错误";
    public static final String SERIALIZE_ERROR="序列化错误";
    public static final String YYYY_MM_DD_HH_MM_SS="yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD="yyyy-MM-dd";
    public static final String HH_MM_SS="HH:mm:ss";

    public static final String ADMIN_NAME="admin";

}
