package indi.simple.pms.constant;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-27 23:32:53
 */
public class CommonResultConstant {

    public static final int SUCCESS=0;

    public static final int FAIL=1;

}
