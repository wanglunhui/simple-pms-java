package indi.simple.pms.controller;

import indi.simple.pms.support.file.LocalFileTemplate;
import indi.simple.pms.util.ServletUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-06-22 22:15:17
 */
@Controller
@RequiredArgsConstructor
public class FileController {

    private final LocalFileTemplate localFileTemplate;

    @GetMapping("/file/{fileName}")
    public void file(@PathVariable("fileName") String fileName, HttpServletResponse response){
        InputStream inputStream=localFileTemplate.download(fileName);
        ServletUtil.writeResponse(inputStream,response, MediaType.IMAGE_PNG_VALUE,false,null);
    }

}
