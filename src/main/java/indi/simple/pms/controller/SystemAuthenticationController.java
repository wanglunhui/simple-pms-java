package indi.simple.pms.controller;

import indi.simple.pms.aop.log.Log;
import indi.simple.pms.auth.PermitAll;
import indi.simple.pms.entity.datatransferobject.SystemLoginDTO;
import indi.simple.pms.entity.viewobject.SystemCaptchaVO;
import indi.simple.pms.entity.viewobject.CommonResultVO;
import indi.simple.pms.entity.viewobject.SystemTokenVO;
import indi.simple.pms.service.SystemAuthenticationService;
import indi.simple.pms.util.UuidUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * 系统认证 控制类
 *
 * @author wanglunhui
 * @since 2021-04-19 21:10:12
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class SystemAuthenticationController {

    private final SystemAuthenticationService systemAuthenticationService;

    @PermitAll
    @GetMapping("/captcha")
    public CommonResultVO<?> getCaptcha() throws IOException {
        String uuid = UuidUtil.randomUUID().toStringSimple();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String captcha=systemAuthenticationService.getCaptcha(uuid,baos);

        return CommonResultVO.success(new SystemCaptchaVO(uuid,"data:image/png;base64," + Base64.getEncoder().encodeToString(baos.toByteArray())));
    }

    @Log(value = "用户登录")
    @PermitAll
    @PostMapping("/login")
    public CommonResultVO<?> login(@RequestBody @Validated SystemLoginDTO systemLoginDTO, HttpServletRequest request) {
        SystemTokenVO systemTokenVO=systemAuthenticationService.login(systemLoginDTO,request);

        return CommonResultVO.success(systemTokenVO);
    }

    @PermitAll // 不需要认证是因为如果恰好在身份信息过期时退出的话会提示身份信息过期，但此时却是已经登录了的
    @DeleteMapping("/logout")
    public CommonResultVO<?> logout(HttpServletRequest request) {
        boolean flag=systemAuthenticationService.logout(request);

        return CommonResultVO.success(flag);
    }

}
