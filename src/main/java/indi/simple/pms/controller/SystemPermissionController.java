package indi.simple.pms.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import indi.simple.pms.aop.log.Log;
import indi.simple.pms.entity.businessobject.JwtUserBO;
import indi.simple.pms.entity.dataobject.SystemPermissionDO;
import indi.simple.pms.entity.datatransferobject.SystemPermissionSearchDTO;
import indi.simple.pms.entity.viewobject.CommonResultVO;
import indi.simple.pms.service.SystemPermissionService;
import indi.simple.pms.util.SecurityUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统权限表 system_permission 控制类
 *
 * @author wanglunhui
 * @since 2021-04-19 21:10:24
 */
@RestController
@RequestMapping("/system/permission")
@Validated
@AllArgsConstructor
public class SystemPermissionController {

    private final SystemPermissionService systemPermissionService;

    @Log("增加权限")
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('PERMISSION_ADD')")
    public CommonResultVO<?> add(@RequestBody @Validated SystemPermissionDO systemPermissionDO) {
        boolean flag = this.systemPermissionService.add(systemPermissionDO);
        return CommonResultVO.success(flag);
    }

    @Log("删除权限")
    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority('PERMISSION_DELETE')")
    public CommonResultVO<?> delete(@RequestBody @NotNull(message = "不能为null!") List<Long> idList) {
        boolean flag = this.systemPermissionService.delete(idList);
        return CommonResultVO.success(flag);
    }

    @Log("修改权限")
    @PutMapping("/update")
    @PreAuthorize("hasAuthority('PERMISSION_UPDATE')")
    public CommonResultVO<?> update(@RequestBody @Validated(SystemPermissionDO.Update.class) SystemPermissionDO systemPermissionDO) {
        boolean flag = this.systemPermissionService.update(systemPermissionDO);
        return CommonResultVO.success(flag);
    }

    @GetMapping("/get/current")
    public CommonResultVO<?> getCurrentPermission() {
        JwtUserBO jwtUserBO = SecurityUtil.getJwtUserBO();
        return CommonResultVO.success(jwtUserBO.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
    }

    @PostMapping("/search/page")
    public CommonResultVO<?> searchPage(@RequestBody SystemPermissionSearchDTO systemPermissionSearchDTO) {
        IPage<SystemPermissionDO> systemPermissionDOIPage = this.systemPermissionService.searchPage(systemPermissionSearchDTO);
        return CommonResultVO.success(systemPermissionDOIPage);
    }

}

