package indi.simple.pms.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import indi.simple.pms.aop.log.Log;
import indi.simple.pms.entity.dataobject.SystemUserDO;
import indi.simple.pms.entity.datatransferobject.SystemChangePasswordDTO;
import indi.simple.pms.entity.datatransferobject.SystemUserSearchDTO;
import indi.simple.pms.entity.datatransferobject.SystemUserUpdateProfileDTO;
import indi.simple.pms.entity.viewobject.CommonResultVO;
import indi.simple.pms.entity.viewobject.JwtUserVO;
import indi.simple.pms.service.SystemUserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 系统用户表 system_user 控制类
 *
 * @author wanglunhui
 * @since 2021-04-19 21:10:36
 */
@RestController
@RequestMapping("/system/user")
@Validated
@AllArgsConstructor
public class SystemUserController {

    private final SystemUserService systemUserService;

    @Log("增加用户")
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('USER_ADD')")
    public CommonResultVO<?> add(@RequestParam("json") @NotBlank(message = "不能为null!") String json, @RequestParam(value="avatar",required = false) MultipartFile avatar) {
        this.systemUserService.add(json,avatar);
        return CommonResultVO.success(true);
    }

    @Log("删除用户")
    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority('USER_DELETE')")
    public CommonResultVO<?> delete(@RequestBody @NotNull(message = "不能为null!") List<Long> idList) {
        boolean flag = this.systemUserService.delete(idList);
        return CommonResultVO.success(flag);
    }

    @Log("修改用户")
    @PutMapping("/update")
    @PreAuthorize("hasAuthority('USER_UPDATE')")
    public CommonResultVO<?> update(@RequestParam("json") @NotBlank(message = "不能为null!") String json, @RequestParam(value="newAvatar",required = false) MultipartFile newAvatar) {
        boolean flag = this.systemUserService.update(json,newAvatar);
        return CommonResultVO.success(flag);
    }

    @PreAuthorize("hasAuthority('USER_GET')")
    @PostMapping("/search/page")
    //@SqlSlot("dataScope()")
    public CommonResultVO<?> searchPageUser(@RequestBody SystemUserSearchDTO systemUserSearchDTO) {
        IPage<SystemUserDO> systemUserDOIPage = this.systemUserService.searchPage(systemUserSearchDTO);
        return CommonResultVO.success(systemUserDOIPage);
    }

    @Log("修改密码")
    @PutMapping("/changePassword")
    public CommonResultVO<?> changePassword(@RequestBody @Validated SystemChangePasswordDTO changePasswordDTO, HttpServletRequest request) {
        boolean flag = this.systemUserService.changePassword(changePasswordDTO, request);
        return CommonResultVO.success(flag);
    }

    @Log("修改头像")
    @PostMapping("/upload/avatar")
    public CommonResultVO<?> uploadAvatar(@RequestParam("userId") @NotNull(message = "不能为null!") Long userId, @RequestParam(value = "file") MultipartFile file) {
        String result = this.systemUserService.uploadAvatar(userId, file);
        return CommonResultVO.success(result);
    }

    @Log("修改Profile")
    @PostMapping("/update/profile")
    public CommonResultVO<?> updateProfile(@RequestBody @Validated SystemUserUpdateProfileDTO systemUserUpdateProfileDTO){
        boolean flag = this.systemUserService.updateProfile(systemUserUpdateProfileDTO);
        return CommonResultVO.success(flag);
    }

    @GetMapping("/get/current")
    public CommonResultVO<?> getCurrent() {
        SystemUserDO systemUserDO = this.systemUserService.getCurrent();
        return CommonResultVO.success(systemUserDO);
    }

    @GetMapping("/get/info")
    public CommonResultVO<?> getInfo(@RequestParam("name") @NotBlank(message = "不能为null!") String name){
        JwtUserVO jwtUserVO = this.systemUserService.getJwtUserVOByName(name);
        return CommonResultVO.success(jwtUserVO);
    }

}
