package indi.simple.pms.entity.viewobject;

import indi.simple.pms.constant.CommonResultConstant;
import lombok.Getter;
import lombok.Setter;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-27 23:32:04
 */
@Getter
@Setter
public class CommonResultVO<T> {

    private Integer code;
    private String message;
    private T data;

    public static <T> CommonResultVO<T> success(){
        return all(CommonResultConstant.SUCCESS,null,null);
    }

    public static <T> CommonResultVO<T> success(Integer code){
        return all(code,null,null);
    }

    public static <T> CommonResultVO<T> success(String message){
        return all(CommonResultConstant.SUCCESS,message,null);
    }

    public static <T> CommonResultVO<T> success(T data){
        return all(CommonResultConstant.SUCCESS,null,data);
    }

    public static <T> CommonResultVO<T> success(Integer code, String message){
        return all(code,message,null);
    }

    public static <T> CommonResultVO<T> success(Integer code, T data){
        return all(code,null,data);
    }

    public static <T> CommonResultVO<T> success(String message, T data){
        return all(CommonResultConstant.SUCCESS,message,data);
    }

    public static <T> CommonResultVO<T> fail(){
        return all(null,null,null);
    }

    public static <T> CommonResultVO<T> fail(Integer code){
        return all(code,null,null);
    }

    public static <T> CommonResultVO<T> fail(String message){
        return all(CommonResultConstant.FAIL,message,null);
    }

    public static <T> CommonResultVO<T> fail(T data){
        return all(CommonResultConstant.FAIL,null,data);
    }

    public static <T> CommonResultVO<T> fail(Integer code, String message){
        return all(code,message,null);
    }

    public static <T> CommonResultVO<T> fail(Integer code, T data){
        return all(code,null,data);
    }

    public static <T> CommonResultVO<T> fail(String message, T data){
        return all(CommonResultConstant.FAIL,message,data);
    }

    public static <T> CommonResultVO<T> all(Integer code, String message, T data){
        CommonResultVO<T> commonResultVO =new CommonResultVO<>();
        commonResultVO.setCode(code);
        commonResultVO.setMessage(message);
        commonResultVO.setData(data);

        return commonResultVO;
    }

}
