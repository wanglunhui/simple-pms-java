package indi.simple.pms.entity.viewobject;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-06-07 10:18:23
 */
@Getter
@Setter
public class JwtUserVO implements Serializable {

    private static final long serialVersionUID = -4787667102004956633L;

    private Long id;

    private String name;

    private String password;

    private String salt;

    private String enable;

    private Long departmentId;

    private List<Long> roleIdList;

    private List<String> authorities;

}
