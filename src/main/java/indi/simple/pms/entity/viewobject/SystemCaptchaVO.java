package indi.simple.pms.entity.viewobject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wanglunhui
 * @Date: 2021/4/13 22:20
 * @Description:
 */
@Getter
@Setter
@AllArgsConstructor
public class SystemCaptchaVO {

    private String uuid;

    private String captcha;

}
