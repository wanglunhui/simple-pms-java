package indi.simple.pms.entity.viewobject;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-03-30 21:45:31
 */
@Getter
@Setter
public class SystemDashboardVO {

    private Long userCount;

    private Long roleCount;

    private Long departmentCount;

    private Long jobCount;

    private List<Long> thisWeekLogCountList;

}
