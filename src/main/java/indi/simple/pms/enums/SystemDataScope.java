package indi.simple.pms.enums;

import lombok.Getter;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-31 15:12:17
 */
@Getter
public enum SystemDataScope {

    ALL("0","全部"),
    SELF_DEPARTMENT("1","本部门"),
    SELF_DEPARTMENT_AND_BELOW("2","本部门及以下部门"),
    SELF("3","仅本人"),
    CUSTOM("4","自定义");

    private final String code;
    private final String message;


    SystemDataScope(String code, String message) {
        this.code=code;
        this.message=message;
    }

    public static SystemDataScope getByCode(String code){
        for(SystemDataScope systemDataScope:SystemDataScope.values()){
            if(systemDataScope.code.equals(code)){
                return systemDataScope;
            }
        }

        throw new RuntimeException("cannot get SystemDataScope by code: "+code);
    }


}
