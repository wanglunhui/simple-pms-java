package indi.simple.pms.enums;

import lombok.Getter;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-31 15:12:17
 */
@Getter
public enum SystemLogType {

    OPERATE("0","操作日志"),
    OTHER("1","其他日志");

    private final String code;
    private final String message;


    SystemLogType(String code, String message) {
        this.code=code;
        this.message=message;
    }


}
