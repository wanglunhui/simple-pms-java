package indi.simple.pms.enums;

import lombok.Getter;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-31 15:12:17
 */
@Getter
public enum SystemSexType {

    MALE("0","男"),
    FEMALE("1","女"),
    UNKNOWN("2","未知");

    private final String code;
    private final String message;


    SystemSexType(String code, String message) {
        this.code=code;
        this.message=message;
    }


}
