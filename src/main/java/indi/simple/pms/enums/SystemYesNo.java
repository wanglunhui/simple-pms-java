package indi.simple.pms.enums;

import lombok.Getter;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-31 15:12:17
 */
@Getter
public enum SystemYesNo {

    NO("0","否"),
    YES("1","是");

    private final String code;
    private final String message;


    SystemYesNo(String code, String message) {
        this.code=code;
        this.message=message;
    }

    public static boolean isYes(String code){
        return YES.code.equals(code);
    }

}
