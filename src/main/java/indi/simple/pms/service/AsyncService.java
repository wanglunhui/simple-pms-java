package indi.simple.pms.service;

import indi.simple.pms.aop.log.Log;
import indi.simple.pms.aop.log.LogLevel;
import indi.simple.pms.constant.CommonConstant;
import indi.simple.pms.entity.dataobject.SystemLogDO;
import indi.simple.pms.entity.datatransferobject.SystemLoginDTO;
import indi.simple.pms.enums.SystemLogType;
import indi.simple.pms.util.JsonUtil;
import indi.simple.pms.util.RequestUtil;
import indi.simple.pms.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.locks.AbstractQueuedLongSynchronizer;

/**
 * @Author: wanglunhui
 * @Date: 2021/6/27 23:18
 * @Description:
 */
@Service
public class AsyncService {

    private static final Logger LOGGER= LoggerFactory.getLogger(AsyncService.class);

    @Autowired
    private SystemLogService systemLogService;

    @Async
    public void saveSysLog(JoinPoint joinPoint, LogLevel logLevel, Throwable e, ThreadLocal<LocalDateTime> localDateTimeThreadLocal) {
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        Method method = signature.getMethod();
        HttpServletRequest request = RequestUtil.getHttpServletRequest();
        SystemLogDO systemLogDO = new SystemLogDO();
        Log log = method.getAnnotation(Log.class);
        systemLogDO.setName(log.value());
        StringBuilder params = new StringBuilder("{");
        Object[] argValues = joinPoint.getArgs();
        String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
        Class<?>[] types =  ((MethodSignature)joinPoint.getSignature()).getParameterTypes();
        if (argValues != null) {
            for(int i = 0; i < argValues.length; ++i) {
                if (types[i]!=HttpServletRequest.class&&types[i]!=HttpServletResponse.class) {
                    if (types[i]==MultipartFile.class&&argValues[i]!=null) {
                        params.append(" ").append(argNames[i]).append(": ").append(((MultipartFile)argValues[i]).getOriginalFilename());
                    } else {
                        params.append(" ").append(argNames[i]).append(": ").append(JsonUtil.objectToJson(argValues[i]));
                    }
                }
            }
        }

        systemLogDO.setParams(params + "}");
        Optional<String> username = Optional.of(SecurityUtil.getJwtUserBO().getUsername());

        systemLogDO.setUsername(username.orElse(""));
        systemLogDO.setUrl(request.getRequestURI());
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        systemLogDO.setMethod(className + "." + methodName + "()");
        systemLogDO.setTime(Duration.between(localDateTimeThreadLocal.get(), LocalDateTime.now()).toMillis());
        String ip = RequestUtil.getIP(request);
        systemLogDO.setIp(ip);
        systemLogDO.setLogType(SystemLogType.OPERATE.getCode());
        systemLogDO.setException(e != null ? e.getMessage() : null);
        systemLogDO.setCreateTime(localDateTimeThreadLocal.get());

        this.systemLogService.save(systemLogDO);

    }

}
