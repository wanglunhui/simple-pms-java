package indi.simple.pms.util;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.Map;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-04-21 10:57:22
 */
@Component
@RequiredArgsConstructor
public class EmailUtil {

    @Value("${spring.mail.enable}")
    private boolean enable;

    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    /**
     * 发送简单的文件邮件
     * @param subject 主题
     * @param from    发件人
     * @param to      收件人
     * @param cc      抄送人，可以有多个抄送人
     * @param bcc     隐秘抄送人，可以有多个
     * @param date    发送时间
     * @param text    邮件的正文
     */
    public void sendTextMail(String subject, String from, String to, String[] cc, String[] bcc, Date date, String text) {
        if(!enable){
            return;
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(subject);
        message.setFrom(from);
        message.setTo(to);
        if (cc != null) {
            message.setCc(cc);
        }
        if (bcc != null) {
            message.setBcc(bcc);
        }
        message.setSentDate(date);
        message.setText(text);
        try {
            javaMailSender.send(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 发送带附件的邮件
     * @param subject        主题
     * @param from           发件人
     * @param to             收件人
     * @param cc             抄送人，可以有多个抄送人
     * @param bcc            隐秘抄送人，可以有多个
     * @param date           发送时间
     * @param text           邮件的正文
     * @param attachmentName 附件名
     * @param attachmentFile 附件
     */
    @SneakyThrows
    public void sendAttachmentMail(String subject, String from, String to, String[] cc, String[] bcc, Date date, String text, String attachmentName, File attachmentFile) {
        if(!enable){
            return;
        }
        // 1. 构建邮件对象，注意，这里要通过 javaMailSender 来获取一个复杂邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // 2. MimeMessageHelper 是一个邮件配置的辅助工具类，true 表示构建一个 multipart message 类型的邮件
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        // 3. 针对工具类，配置邮件发送的基本信息
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        if (cc != null) {
            helper.setCc(cc);
        }
        if (bcc != null) {
            helper.setBcc(bcc);
        }
        helper.setSentDate(date);
        helper.setText(text);

        // 4. 添加邮件附件
        helper.addAttachment(attachmentName, attachmentFile);
        // 5. 发送邮件
        try {
            javaMailSender.send(mimeMessage);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 发送邮件使用Thymeleaf模板
     *
     * @param subject      主题
     * @param from         发件人
     * @param to           收件人
     * @param cc           抄送人，可以有多个抄送人
     * @param bcc          隐秘抄送人，可以有多个
     * @param date         发送时间
     * @param map         邮件模板需要替换的数据
     * @param templatePath 模板路径
     */
    @SneakyThrows
    public void sendThymeleafMail(String subject, String from, String to, String[] cc, String[] bcc, Date date, Map<String, Object> map, String templatePath) {
        if(!enable){
            return;
        }
        // 1. 构建邮件对象，注意，这里要通过 javaMailSender 来获取一个复杂邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // 2. MimeMessageHelper 是一个邮件配置的辅助工具类，true 表示构建一个 multipart message 类型的邮件
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        // 3. 针对工具类，配置邮件发送的基本信息
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        if (cc != null) {
            helper.setCc(cc);
        }
        if (bcc != null) {
            helper.setBcc(bcc);
        }
        helper.setSentDate(date);

        Context context = new Context();
        if (map != null) {
            map.forEach(context::setVariable);
        }
        String process = templateEngine.process(templatePath, context); // 这个process就是html，可以换成我们自己写的html
        helper.setText(process, true);
        try {
            javaMailSender.send(mimeMessage);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
