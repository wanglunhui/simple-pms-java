package indi.simple.pms.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import indi.simple.pms.constant.CommonConstant;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;

/**
 * @Author: wanglunhui
 * @Date: 2021/4/13 22:39
 * @Description:
 */
public class SerializableUtil {

    public static byte[] serialize(Object object) {
        if (null == object) {
            return null;
        } else {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(object);
                return baos.toByteArray();
            } catch (Exception e) {
                throw new RuntimeException(CommonConstant.SERIALIZE_ERROR, e);
            }
        }
    }

    public static Object deserialize(byte[] bytes) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(Objects.requireNonNull(bytes));
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            throw new RuntimeException(CommonConstant.SERIALIZE_ERROR, e);
        }
    }

}

