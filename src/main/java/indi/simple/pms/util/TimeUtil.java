package indi.simple.pms.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * 类
 *
 * @author wanglunhui
 * @since 2022-05-17 09:27:14
 */
public class TimeUtil {

    @Getter
    @Setter
    @AllArgsConstructor
    public static class TimeDiff{
        private long years;
        private long months;
        private long days;
        private long hours;
        private long minutes;
        private long seconds;
    }

    public static TimeDiff localDateTimeDiff(LocalDateTime start,LocalDateTime end){
        long years=start.until(end, ChronoUnit.YEARS);
        start=start.plusYears(years);

        long months=start.until(end,ChronoUnit.MONTHS );
        start=start.plusMonths(months);

        long days=start.until(end,ChronoUnit.DAYS);
        start=start.plusDays(days);


        long hours=start.until(end,ChronoUnit.HOURS);
        start=start.plusHours(hours);

        long minutes=start.until(end,ChronoUnit.MINUTES);
        start=start.plusMinutes(minutes);

        long seconds=start.until(end,ChronoUnit.SECONDS);

        return new TimeDiff(years,months,days,hours,minutes,seconds);
    }

    public static void main(String[] args) {
        LocalDateTime start=LocalDateTime.of(2021,1,1,1,1,1);
        LocalDateTime end=LocalDateTime.of(2022,2,2,2,2,2);

        TimeDiff timeDiff=localDateTimeDiff(start,end);
        System.out.println(timeDiff.years+"-"+timeDiff.months+"-"+timeDiff.days+" "+timeDiff.hours+":"+timeDiff.minutes+":"+timeDiff.seconds);
    }

}
